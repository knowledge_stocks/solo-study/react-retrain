import React from 'react';

import BatchingTest from './react18/BatchingTest';
import TransisionTest from './react18/TransisionTest';
import ContextTest from './context/ContextTest';
import ReduxTest from './redux/ReduxTest';

function App() {
  return (
    <div className='App'>
      <BatchingTest />
      <TransisionTest />
      <ContextTest />
      <ReduxTest />
    </div>
  );
}

export default App;
