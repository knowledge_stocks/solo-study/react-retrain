import React, { useReducer, createContext, Dispatch, useContext } from 'react';

type Action = { type: 'INCREMENT' } | { type: 'DECREMENT' };

function reducer(state: number, action: Action): number {
  switch (action.type) {
    case 'INCREMENT':
      return state + 1;
    case 'DECREMENT':
      return state - 1;
    default:
      return state;
  }
}

const StateContext = createContext<number>(0);
const DispatchContext = createContext<Dispatch<Action> | null>(null);

type CountProviderProps = {
  children: React.ReactNode;
};

export default function CountProvider({ children }: CountProviderProps) {
  const [state, dispatch] = useReducer(reducer, 0);
  return (
    <StateContext.Provider value={state}>
      <DispatchContext.Provider value={dispatch}>
        {children}
      </DispatchContext.Provider>
    </StateContext.Provider>
  );
}

export function useStateContext() {
  const state = useContext(StateContext);
  if (state == null) throw new Error('Cannot find CountProvider'); // 유효하지 않을땐 에러를 발생
  return state;
}

export function useDispatchContext() {
  const dispatch = useContext(DispatchContext);
  if (!dispatch) throw new Error('Cannot find CountProvider'); // 유효하지 않을땐 에러를 발생
  return dispatch;
}
