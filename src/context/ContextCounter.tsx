import React from 'react';

import { useStateContext, useDispatchContext } from './CountContext';

function ContextCounter() {
  const number = useStateContext();
  const dispatch = useDispatchContext();

  const onIncrease = () => {
    dispatch({ type: 'INCREMENT' });
  };

  const onDecrease = () => {
    dispatch({ type: 'DECREMENT' });
  };

  return (
    <div>
      <h1>{number}</h1>
      <button onClick={onIncrease}>+1</button>
      <button onClick={onDecrease}>-1</button>
    </div>
  );
}

export default ContextCounter;
