import React from 'react';

import CountProvider from './CountContext';
import ContextCounter from './ContextCounter';

function ContextTest() {
  return (
    <>
      <CountProvider>
        <ContextCounter />
      </CountProvider>
      <CountProvider>
        <ContextCounter />
      </CountProvider>
    </>
  );
}

export default ContextTest;
