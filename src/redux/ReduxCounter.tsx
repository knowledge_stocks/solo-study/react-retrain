import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { increment, decrement } from './actions/count_actions';
import { CountState } from './reducers/count_reducer';

function ReduxCounter() {
  const dispatch = useDispatch();

  const number = useSelector((state: CountState) => state.count1);

  const onIncrease = (): void => {
    dispatch(increment());
  };

  const onDecrease = (): void => {
    dispatch(decrement());
  };

  return (
    <div>
      <h1>{number}</h1>
      <button onClick={onIncrease}>+1</button>
      <button onClick={onDecrease}>-1</button>
    </div>
  );
}

export default ReduxCounter;
