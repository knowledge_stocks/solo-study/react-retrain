import { INCREMENT2, DECREMENT2 } from '../actions/types';
import { CountAction } from '../actions/count2_actions';

export type CountState = {
  count1: number;
  count2: number;
};

const initialState: CountState = { count1: 0, count2: 10 };

export default function reducer(
  state: CountState = initialState,
  action: CountAction
): CountState {
  switch (action.type) {
    case INCREMENT2:
      return { ...state, count2: state.count2 + 1 };
    case DECREMENT2:
      return { ...state, count2: state.count2 - 1 };
    default:
      return state;
  }
}
