import { INCREMENT, DECREMENT } from '../actions/types';
import { CountAction } from '../actions/count_actions';

export type CountState = {
  count1: number;
  count2: number;
};

const initialState: CountState = { count1: 10, count2: 5 };

export default function reducer(
  state: CountState = initialState,
  action: CountAction
): CountState {
  switch (action.type) {
    case INCREMENT:
      return { ...state, count1: state.count1 + 1 };
    case DECREMENT:
      return { ...state, count1: state.count1 - 1 };
    default:
      return state;
  }
}
