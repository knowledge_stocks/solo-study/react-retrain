import { INCREMENT, DECREMENT } from './types';

export function increment() {
  return {
    type: INCREMENT,
  };
}

export function decrement() {
  return {
    type: DECREMENT,
  };
}

export type CountAction =
  | ReturnType<typeof increment>
  | ReturnType<typeof decrement>;
