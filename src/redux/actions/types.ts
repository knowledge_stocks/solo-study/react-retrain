// string 타입에도 as const를 붙이면 값 자체가 리터럴로 추론된다.
// 예를들어 let bar = INCREMENT; 에서 bar에 마우스를 가져다대면
// as const를 붙인 경우 'INCREMENT'로 추론되는 반면,
// 안 붙인 경우에는 그냥 string으로 추론된다.

export const INCREMENT = 'INCREMENT' as const;
export const DECREMENT = 'DECREMENT' as const;

export const INCREMENT2 = 'INCREMENT2' as const;
export const DECREMENT2 = 'DECREMENT2' as const;
