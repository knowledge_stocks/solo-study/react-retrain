import { INCREMENT2, DECREMENT2 } from './types';

export function increment() {
  return {
    type: INCREMENT2,
  };
}

export function decrement() {
  return {
    type: DECREMENT2,
  };
}

export type CountAction =
  | ReturnType<typeof increment>
  | ReturnType<typeof decrement>;
