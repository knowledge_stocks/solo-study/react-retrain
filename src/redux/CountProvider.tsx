import React, { useMemo } from 'react';

import { Provider } from 'react-redux';
import { createStore } from 'redux';
import Reducer from './reducers/count_reducer';
import { composeEnhancers } from './reduxDevtools';

type CountProviderProps = {
  children: React.ReactNode;
};

export default function CountProvider({ children }: CountProviderProps) {
  // store를 여러군데에서 만들었을 때 어떻게 동작하는지 테스트해보기 위해서
  const store = useMemo(() => {
    return createStore(Reducer, composeEnhancers());
  }, []);

  return <Provider store={store}>{children}</Provider>;
}
