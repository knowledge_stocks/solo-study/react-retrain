import React from 'react';

import CountProvider from './CountProvider';
import ReduxCounter from './ReduxCounter';

import { Provider } from 'react-redux';
import { createStore } from 'redux';
import Reducer from './reducers/count2_reducer';
import { composeEnhancers } from './reduxDevtools';

const store = createStore(Reducer, composeEnhancers());

function ReduxTest() {
  return (
    <>
      {/* Provider가 두개 중첩될 경우 어떻게 동작하는지 테스트용 */}
      <Provider store={store}>
        <CountProvider>
          <ReduxCounter />
        </CountProvider>
        <CountProvider>
          <ReduxCounter />
        </CountProvider>
      </Provider>
    </>
  );
}

export default ReduxTest;
