import React, { useState, Suspense, useTransition } from 'react';
import ReactDOM from 'react-dom';

import { fetchProfileData } from './fakeApi';

function getNextId(id) {
  return id === 3 ? 0 : id + 1;
}

const initialResource = fetchProfileData(0);

function TransisionTest() {
  const [resource, setResource] = useState(initialResource);
  const [isPending, startTransition] = useTransition({
    timeoutMs: 3000,
  });
  return (
    <div>
      <button
        onClick={() => {
          // startTransition을 사용하게 되면 timeoutMs에 입력된 3초동안 Suspense의 fallback으로 바로 넘어가지 않고 일단 이전 화면을 유지한다.
          // 로드 시간이 timeoutMs보다 늦어질 경우 Suspense의 fallback이 나타나 로딩 표기를 하게 된다.
          // 즉, startTransition은 화면의 렌더를 늦추는 역할을 수행한다고 보면 될 것 같다.
          //
          // # transition의 흐름
          // 1. setResource로 resource를 변경한다. // 아직 resource의 내부 Promise들은 pending 상태
          // 2. resource가 변경되었으므로 화면을 다시 렌더링 해야하지만
          // 아직 Promise가 끝나지 않았기 때문에 내부 값을 참조하는 부분에서 exeption들이 발생하므로 Suspense의 fallback 상태
          // 3. Suspense fallback 상태이지만 아직 timeoutMs에 지정한 3초가 지나지 않았기 때문에 기존 화면을 유지
          // 4. 3초가 지나면 기존 화면을 제거하고 Suspense fallback에 지정한 화면으로 렌더
          // 5. resource 내부의 Promise들이 종료되어 하위 컴포넌트들이 정상 로딩되면 하위 컴포넌트들을 렌더
          startTransition(() => {
            const nextUserId = getNextId(resource.userId);
            setResource(fetchProfileData(nextUserId));
          });
        }}
      >
        Next
      </button>
      <ProfilePage resource={resource} />
    </div>
  );
}

function ProfilePage({ resource }) {
  return (
    <Suspense fallback={<h1>Loading profile...</h1>}>
      <ProfileDetails resource={resource} />
      <Suspense fallback={<h1>Loading posts...</h1>}>
        <ProfileTimeline resource={resource} />
      </Suspense>
    </Suspense>
  );
}

function ProfileDetails({ resource }) {
  const user = resource.user.read();
  return <h1>{user.name}</h1>;
}

function ProfileTimeline({ resource }) {
  const posts = resource.posts.read();
  return (
    <ul>
      {posts.map((post) => (
        <li key={post.id}>{post.text}</li>
      ))}
    </ul>
  );
}

export default TransisionTest;
