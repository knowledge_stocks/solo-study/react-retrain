import React, { useState, useCallback, useLayoutEffect } from 'react';
import { flushSync } from 'react-dom';

function sleep(ms: number): void {
  const wakeUpTime = Date.now() + ms;
  while (Date.now() < wakeUpTime) {}
}

// 렌더가 수행되는지 확인하기 위한 Hook
function LogEvents(): null {
  useLayoutEffect(() => {
    console.log('Commit');
  });
  console.log('Render');
  return null;
}

function BatchingTest() {
  const [state1, setState1] = useState<number>(0);
  const [state2, setState2] = useState<boolean>(true);
  const [input, setInput] = useState<string>('');
  const [lazyInput, setLazyInput] = useState<string>('');

  const testBatching = useCallback(() => {
    // setState1((state1) => state1 + 1);
    // setState2((state2) => !state2);
    new Promise((resolve) => setTimeout(resolve, 100)).then(() => {
      flushSync(() => {
        // flushSync를 사용하면 Automatic Bathcing를 사용하지 않는다.
        // 하지만 flushSync를 내부에서 여러번 setState를 하는 경우에는 Batching이 적용된다.
        setState1((state1) => state1 + 1);
        setState2((state2) => !state2);
      });
      // ReactDOM.render를 그대로 사용하면 2번씩 렌더링된다.
      // ReactDOM.createRoot를 사용하면 Automatic Bathcing으로 한 번만 렌더링 된다.
      // 근데 ReactDOM.createRoot는 아직 나온지 얼마 안 되어서 그런가? 자동 완성이 안 뜬다.
    });
  }, []);

  return (
    <div>
      <button onClick={testBatching}>Test Batching</button>
      <LogEvents />
    </div>
  );
}

export default BatchingTest;
